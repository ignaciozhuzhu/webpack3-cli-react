import { Router, Route, hashHistory } from 'react-router';

//患者入口
import MobileIndex from './components/mobile_index';

import {
  getHtmlFontSize
} from './lib/common.js';

export default class Root extends React.Component {
  render() {
    getHtmlFontSize();
    return (
      <div>
          <Router history={hashHistory}>
            <Route path="/" component={ MobileIndex }></Route>
          </Router>
      </div>
    );
  };
}

ReactDOM.render(<Root/>, document.getElementById('mainContainer'));