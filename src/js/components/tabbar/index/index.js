import { TabBar, WhiteSpace } from 'antd-mobile';
import { Tabs } from 'antd';
import './index.less';

const TabPane = Tabs.TabPane;
const HOMETAB = 'homeTab'

export default class TabBarCPG extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedTab: this.getSelectedTab(),
      hidden: false,
      fullScreen: true,
    };
  }
  getSelectedTab() {
    let sTab = localStorage.getItem('SelectedTab');
    if (sTab) return sTab
    else {
      localStorage.setItem('SelectedTab', HOMETAB);
      sTab = localStorage.getItem('SelectedTab');
      return sTab
    }

  }

  renderContent(pageText) {
    return (
      <div style={{ height: '100%', textAlign: 'center' }}>
        {pageText=="公司简介"?"公司简介":null}
        {pageText=="首页"?"首页":null}
        {pageText=="公司名片"?"公司名片":null}
      </div>
    );
  }

  render() {
    let tabBarJson = [{
        "title": "公司简介",
        "key": "公司简介",
        "renderContent": "公司简介",
        "badge": "",
        "icon": "tvshow",
        "selectedIcon": "tvshow-selected",
        "selectedState": "tvTab",
        "dataSeed": "tvshowId",
      }, {
        "title": "首页",
        "key": "首页",
        "renderContent": "首页",
        "badge": "",
        "icon": "home",
        "selectedIcon": "home-selected",
        "selectedState": "homeTab",
        "dataSeed": "homeId",
      }, {
        "title": "公司名片",
        "key": "公司名片",
        "renderContent": "公司名片",
        "badge": "",
        "icon": "my",
        "selectedIcon": "my-selected",
        "selectedState": "myTab",
        "dataSeed": "myId",
      }

    ]
    let that = this;
    let TabBarItem = tabBarJson.map(function (tabBar) {
      return (
        <TabBar.Item
            title={tabBar.title}
            key={tabBar.key}
            badge={tabBar.badge}
            icon={<div className={tabBar.icon}></div>}
            selectedIcon={<div className={tabBar.selectedIcon}></div>} 
            selected={that.state.selectedTab === tabBar.selectedState}
            onPress={() => {
              that.setState({
                selectedTab: tabBar.selectedState,
              });
              document.title = tabBar.title
              localStorage.setItem('SelectedTab', tabBar.selectedState);
            }}
            data-seed={tabBar.dataSeed}
          >
            {that.renderContent(tabBar.renderContent)}
          </TabBar.Item>
      );
    })
    return (
      <div><div style={that.state.fullScreen ? { position: 'fixed', height: '100%', width: '100%', top: 0 } : { height: 400 }}>
        <TabBar
          unselectedTintColor="#949494"
          tintColor="#FC7935"
          barTintColor="white"
          hidden={that.state.hidden}
        >
        {TabBarItem}
        </TabBar>
      </div></div>
    )

  }
}