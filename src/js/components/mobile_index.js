import TabBarCPG from './tabbar/index';

import { disableGetDefaultPropsWarning } from '../lib/common'

export default class MobileIndex extends React.Component {
  render() {
    disableGetDefaultPropsWarning()
    return (
      <div>
		<TabBarCPG />
	  </div>
    );
  }
}