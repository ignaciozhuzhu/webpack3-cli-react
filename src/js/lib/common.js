/**
 * 将日期时间格式转化为xx月xx日格式
 * 2017-11-07 00:00:00  =>  11月07日
 * Created by 龙鸿轩 on 2017/12/15.
 */
//
export const getMobilTime = (datetime) => datetime.substr(5, 2) + "月" + datetime.substr(8, 2) + "日"

/*
 * 获取适配宽度比例
 * 默认为 设计稿竖直放时的横向分辨率为640px--> pert: 6.4    px/70=rem
 * Created by 龙鸿轩 on 2017/12/15.
 */
export const getHtmlFontSize = (pert) => {
  //setopenid();
  pert = pert || 5.4;
  let cWidth = document.documentElement.clientWidth / pert;
  document.documentElement.style.fontSize = cWidth + 'px';
  //每当浏览器窗口大小发生变化
  $(window).resize(function () {
    pert = pert || 5.4;
    document.documentElement.style.fontSize = cWidth + 'px';
  });
  return cWidth;
}

/*
 * 获取域名
 * Created by 龙鸿轩 on 2017/12/18.
 */
export const getServer = () => {
  let server = '';
  if (process.env.NODE_ENV.trim() == "production")
    server = 'https://www.mymengqiqi.com'
  if (process.env.NODE_ENV.trim() == "dev")
    server = 'http://192.168.1.122:8081'
  return server
}
export const get7niuServer = () => {
    return 'http://7vikwk.com1.z0.glb.clouddn.com'
  }
  /**
   * 代理api路径
   * Created by 龙鸿轩 on 2017/12/25.
   */
export const apiserver = process.env.NODE_ENV.trim() == 'dev' ? 'api/' : getServer()


/**
 * ignore react Warning
 * Created by 龙鸿轩 on 2017/12/18.
 */
export const disableGetDefaultPropsWarning = () => {
  const warning = "Warning: getDefaultProps is only used on classic React.createClass definitions. Use a static property named `defaultProps` instead.";
  const consoleError = console.error.bind(console);

  console.error = (...args) => {
    if (args[0] === warning) return;
    return consoleError(...args);
  };
};

/**
 * 时间秒数格式化
 * @param s 时间戳（单位：秒）
 * @returns {*} 格式化后的时分秒
 * Created by 龙鸿轩 on 2017/12/20.
 */
export const sec_to_time = (s) => {
  var t;
  if (s > -1) {
    var hour = Math.floor(s / 3600);
    var min = Math.floor(s / 60) % 60;
    var sec = s % 60;
    if (hour < 10) {
      t = '0' + hour + ":";
    } else {
      t = hour + ":";
    }

    if (min < 10) { t += "0"; }
    t += min + ":";
    if (sec < 10) { t += "0"; }
    t += sec.toFixed(0);
  }
  return t;
}

/**
 * 时分秒格式 转化为 Date格式
 * Created by 龙鸿轩 on 2017/12/20.
 */
export const str_to_date = (str) => {
  return new Date(Date.parse(str.replace(/-/g, "/")));
}

/**
 * Date 时间比较 return 时间差的秒数
 * 结束时间若早于开始时间,返回0
 * Created by 龙鸿轩 on 2017/12/20.
 */
export const date_diff = (dateEnd, dateStart) => {
  return (dateEnd.getTime() - dateStart.getTime() > 0 ? dateEnd.getTime() - dateStart.getTime() : 0) / 1000
}

export const setCookie = (c_name, value, expiredays) => {
  var exdate = new Date()
  exdate.setDate(exdate.getDate() + expiredays)
  document.cookie = c_name + "=" + escape(value) +
    ((expiredays == null) ? "" : ";expires=" + exdate.toGMTString())
}

export const getCookie = (c_name) => {
  if (document.cookie.length > 0) {
    let c_start = document.cookie.indexOf(c_name + "=")
    if (c_start != -1) {
      c_start = c_start + c_name.length + 1
      let c_end = document.cookie.indexOf(";", c_start)
      if (c_end == -1) c_end = document.cookie.length
      return unescape(document.cookie.substring(c_start, c_end))
    }
  }
  return ""
}


export const add0 = (time) => {
  var oldTime = new Date(time);
  var year = oldTime.getFullYear();
  var mon = oldTime.getMonth() + 1;
  var dd = oldTime.getDate();
  var hh = oldTime.getHours();
  var mm = oldTime.getMinutes();
  var ss = oldTime.getSeconds();
  if (mon < 10) {
    mon = "0" + mon;
  }
  if (dd < 10) {
    dd = "0" + dd;
  }
  if (hh < 10) {
    hh = "0" + hh;
  }
  if (mm < 10) {
    mm = "0" + mm;
  }
  if (ss < 10) {
    ss = "0" + ss;
  }
  return year + "-" + mon + "-" + dd + "  " + hh + ":" + mm + ":" + ss;
}



/**
 * 获取query string id
 * demo: 'http://192.168.6.113:8081/#/evdetails/256?_k=bx2qd3' , prename=evdetails 
 *       => 256
 * Created by 龙鸿轩 on 2017/12/28.
 */
export const getQueryByPre = (prename) => {
  let href = window.location.href
  return href.substring(href.indexOf(prename) + prename.length + 1, href.lastIndexOf('?'))
}

/**
 * content内容过长截断。
 * Created by 龙鸿轩 on 2018/1/17.
 */
export const textSubStr = (text, len = 48) => {
  return text.length > len ? text.substr(0, len) + '...' : text
}


/**
 * fetch 请求option对象,传输cookie
 * Created by 龙鸿轩 on 2017/12/25.
 //var myHeaders = new Headers();
 //myHeaders.append("Content-Type", "application/json");
 //myHeaders.append("Content-Type", "application/x-www-form-urlencoded");
 //myHeaders.append("X-Custom-Header", "ProcessThisImmediately");
 //myHeaders.append("Content-Length", content.length.toString()); 2018/01/08 update
 // headers: myHeaders,
 */
export const FetchOptionsG = {
  method: 'GET',
  credentials: 'include',
};

export const FetchOptionsP = {
  method: 'POST',
  credentials: 'include',
};

/**
 * 获取sig。
 * Created by 龙鸿轩 on 2018/2/18.
 */
export const getSig = () => {
  var sig = "";
  var getsignatureNew = function (url) {
      let ajaxUrl = ""
      ajaxUrl = process.env.NODE_ENV.trim() == "dev" ?
        apiserver + "/mhshi/ajax/demoHandler.ashx?fn=getsig&url=" + url + "" :
        getServer() + "/mhshi/ajax/demoHandler.ashx?fn=getsig&url=" + url + ""
      $.ajax({
        url: ajaxUrl,
        dataType: 'json',
        contentType: "multipart/form-data",
        type: 'GET',
        async: false,
        success: function (data) {},
        error: function (XMLHttpRequest) {
          sig = XMLHttpRequest.responseText;
          return;
        }
      })
    }
    //if (isWeiXin) {
  let url = window.location.href
  url = window.location.href.split('#')[0]
  getsignatureNew(url);
  return sig
}

/**
 * 微信config配置
 * Created by 龙鸿轩 on 2018/02/04.
 */
export const wxConfig = () => {
  let sig = getSig()
  wx.config({
    debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
    appId: 'wxbf5afc94a591cdd8', // 必填，公众号的唯一标识
    timestamp: '1387970259', // 必填，生成签名的时间戳
    nonceStr: 'zhusheng123', // 必填，生成签名的随机串
    signature: sig, // 必填，签名，见附录1
    jsApiList: [
        'checkJsApi',
        'onMenuShareTimeline',
        'onMenuShareAppMessage',
        'onMenuShareQQ',
        'onMenuShareWeibo',
        'onMenuShareQZone',
        'hideMenuItems',
        'showMenuItems',
        'hideAllNonBaseMenuItem',
        'showAllNonBaseMenuItem',
        'translateVoice',
        'startRecord',
        'stopRecord',
        'onVoiceRecordEnd',
        'playVoice',
        'onVoicePlayEnd',
        'pauseVoice',
        'stopVoice',
        'uploadVoice',
        'downloadVoice',
        'chooseImage',
        'previewImage',
        'uploadImage',
        'downloadImage',
        'getNetworkType',
        'openLocation',
        'getLocation',
        'hideOptionMenu',
        'showOptionMenu',
        'closeWindow',
        'scanQRCode',
        'chooseWXPay',
        'openProductSpecificView',
        'addCard',
        'chooseCard',
        'openCard'
      ] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
  });
  let title = "标题";
  let desc = "描述";
  let imgUrl = "http://wx.qlogo.cn/mmopen/vi_32/iahBszFA08qfpEeFfk7pMQjz0hchb7I16VMMbcS0USg0niaeRRTtfWtqSfdRlc8ZTZMuSqGfuZvA8SLeIJnXgytQ/132";
  wx.ready(function () {
    wx.onMenuShareTimeline({
      title: title, // 分享标题
      desc: desc, //分享描述
      //link: link,
      imgUrl: imgUrl, // 分享图标
      success: function () {
        // 用户确认分享后执行的回调函数
      },
      cancel: function () {
        // 用户取消分享后执行的回调函数
      }
    });
    wx.onMenuShareAppMessage({
      title: title, // 分享标题
      desc: desc, // 分享描述
      //link: link,
      imgUrl: imgUrl, // 分享图标
      success: function () {
        // 用户确认分享后执行的回调函数
      },
      cancel: function () {
        // 用户取消分享后执行的回调函数
      }
    });
  })
}
export const order = () => {
  wx.request({
    url: 'https://api.mch.weixin.qq.com/pay/unifiedorder',
    data: {
      appid: 'wxbf5afc94a591cdd8',
      mch_id: 'mehaoshi',
      nonceStr: "zhusheng123",
      sign: getSig(),
      body: 'miaoshu',
      out_trade_no: '123',
      total_fee: '0.01',
      spbill_create_ip: '127.0.0.1',
      notify_url: 'https://www.mymengqiqi.com',
      trade_type: 'JSAPI',
    },
    header: { 'content-type': 'application/json' },
    method: 'POST',
    dataType: 'json',
    success: function (res) {
      alert(res.data)
    }
  })
}

/*export const onBridgeReady = () => {

  WeixinJSBridge.invoke(
    'getBrandWCPayRequest', {
      "appId": "wxbf5afc94a591cdd8", //公众号名称，由商户传入     
      "timeStamp": "1387970259", //时间戳，自1970年以来的秒数     
      "nonceStr": "zhusheng123", //随机串     
      "package": "prepay_id=wx201802182009395522657a690389285133",
      "signType": "MD5", //微信签名方式：     
      "paySign": getSig() //微信签名 
    },
    function(res) {
      if (res.err_msg == "get_brand_wcpay_request:ok") {} // 使用以上方式判断前端返回,微信团队郑重提示：res.err_msg将在用户支付成功后返回    ok，但并不保证它绝对可靠。 
    }
  );
}
if (typeof WeixinJSBridge == "undefined") {
  if (document.addEventListener) {
    document.addEventListener('WeixinJSBridgeReady', onBridgeReady, false);
  } else if (document.attachEvent) {
    document.attachEvent('WeixinJSBridgeReady', onBridgeReady);
    document.attachEvent('onWeixinJSBridgeReady', onBridgeReady);
  }
} else {
  onBridgeReady();
}
*/
export const getNowFormatDate = (date = new Date()) => {
  //var date = new Date();
  var seperator1 = "-";
  var year = date.getFullYear();
  var month = date.getMonth() + 1;
  var strDate = date.getDate();

  var hh = date.getHours();
  var mm = date.getMinutes();
  var ss = date.getSeconds();

  if (month >= 1 && month <= 9) {
    month = "0" + month;
  }
  if (strDate >= 0 && strDate <= 9) {
    strDate = "0" + strDate;
  }

  if (hh < 10) {
    hh = "0" + hh;
  }
  if (mm < 10) {
    mm = "0" + mm;
  }
  if (ss < 10) {
    ss = "0" + ss;
  }
  var currentdate = year + seperator1 + month + seperator1 + strDate + "  " + hh + ":" + mm + ":" + ss;
  return currentdate;
}


/**
 * 判断手机号 验证规则：11位数字，以1开头。
 * Created by 龙鸿轩 on 2018/1/17.
 */
export const checkMobile = (str) => {
  var re = /^1\d{10}$/
  if (re.test(str)) {
    return true;
  } else {
    return false
  }
}

/**
 * getQueryString
 * Created by 龙鸿轩 on 2018/2/20.
 */
export const getQueryString = (name) => {
  var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
  var r = window.location.search.substr(1).match(reg);
  if (r != null) {
    return unescape(r[2]);
  }
  return null;
}

/**
 * 根据openid获取用户信息
 * Created by 龙鸿轩 on 2018/2/24.
 */
export const getUserinfoByOpenid = (that) => {
  fetch(apiserver + `/mhshi/ajax/mhshiHandler.ashx?fn=getuserinfobyopenid&openid=${localStorage.getItem('openid')}`, FetchOptionsP)
    .then(response => response.json())
    .then(json => {
      if (json.data.length > 0) {
        console.log('用户信息:', json.data[0])
        that.setState({
          userdata: json.data[0],
        })
      } else {
        that.setState({
          userdata: '',
        })
      }
    }).catch(XHR => console.error("XHR:" + XHR));
}

/**
 * 根据openid参数获取用户信息
 * Created by 龙鸿轩 on 2018/4/9.
 */
export const getUserinfoByid = (that, id) => {
  fetch(apiserver + `/mhshi/ajax/mhshiHandler.ashx?fn=getuserinfobyid&id=${id}`, FetchOptionsP)
    .then(response => response.json())
    .then(json => {
      if (json.data.length > 0) {
        console.log('用户信息:', json.data[0])
        that.setState({
          userdata: json.data[0],
        })
      } else {
        that.setState({
          userdata: '',
        })
      }
    }).catch(XHR => console.error("XHR:" + XHR));
}

/**
 * 根据uid, qid获取录音内容并播放
 * Created by 龙鸿轩 on 2018/3/6.
 */
export const downloadVoice = (uid, qid) => {
  fetch(apiserver + `/mhshi/ajax/mhshiHandler.ashx?fn=getvoice&uid=${uid}&qid=${qid}`, FetchOptionsP)
    .then(response => response.json())
    .then(json => {
      // console.log('downloadVoice', json.data)
      // RongIMLib.RongIMVoice.init();
      // RongIMLib.RongIMVoice.play(json.data[0].base64);

      wx.downloadVoice({
        serverId: json.data[0].voiceUrl, // 需要下载的音频的服务器端ID，由uploadVoice接口获得
        isShowProgressTips: 1, // 默认为1，显示进度提示
        success: function (res) {
          console.log(JSON.stringify(res))
          wx.playVoice({
            localId: res.localId // 需要播放的音频的本地ID，由stopRecord接口获得
          });
        }
      });
      /*      wx.onVoicePlayEnd({
              serverId: json.data[0].voiceUrl, // 需要下载的音频的服务器端ID，由uploadVoice接口获得
              success: function (res) {
                var localId = res.localId; // 返回音频的本地ID
              }
            });*/
    }).catch(XHR => console.log("XHR:" + XHR));

}


/**
 * 根据uid, qid获取录音内容并播放
 * Created by 龙鸿轩 on 2018/3/6.
 */
export const playVoice = (uid, qid, index, flag) => {
  // console.log('index', index)
  fetch(apiserver + `/mhshi/ajax/mhshiHandler.ashx?fn=getvoice&uid=${uid}&qid=${qid}`, FetchOptionsP)
    .then(response => response.json())
    .then(json => {
      console.log('downloadVoice', json.data)
      if (flag == 'cc') {
        $('.cc')[index].src = `https://www.mymengqiqi.com/mhshi/voice/` + json.data[0].voiceUrl
        $('.cc')[index].play()
      }
      if (flag == 'dd') {
        $('.dd')[index].src = `https://www.mymengqiqi.com/mhshi/voice/` + json.data[0].voiceUrl
        $('.dd')[index].play()
      }
    }).catch(XHR => console.log("XHR:" + XHR));
}

/**
 * 设置 localStorage 个人信息 ,主要是 openid ,防止无限登录
 * Created by 龙鸿轩 on 2018/3/22.
 */
export const setLocalUserInfo = () => {
  let code = getQueryString('code')
    // console.log('code', code)
  let appid = 'wxbf5afc94a591cdd8'
  let appsecret = '55ca06fb7ad8762d32e7617cb3f28232'
  let url = apiserver + `/mhshi/ajax/demoHandler.ashx?fn=getuserinfo&appid=${appid}&appsecret=${appsecret}&code=${code}`
  if (code)
    fetch(url, FetchOptionsP)
    .then(response => response.json())
    .then(json => {
      console.log(json)
      localStorage.setItem('openid', json.openid)
      localStorage.setItem('headimgurl', json.headimgurl)
      localStorage.setItem('nickname', json.nickname)
      window.location.href = 'https://www.mymengqiqi.com/mhshi/'
    }).catch(XHR => {
      console.error("XHR:" + XHR)
    })
}