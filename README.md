
使用 React + Webpack + ES6 + antd 进行组合开发的种子项目。

安装 & 运行：

```
git clone https://github.com/ignaciozhuzhu/webpack3-cli-react.git
cd webpack3-cli-react
npm install
npm run dev
```

打开 http://localhost:8081 即可看到效果。